package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
