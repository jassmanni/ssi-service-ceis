package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
