package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.SubCategory;
import org.springframework.data.repository.CrudRepository;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
}
