package edu.ceis.ssiserviceceis.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Position extends ModelBase {

    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {

        return name;
    }

}
